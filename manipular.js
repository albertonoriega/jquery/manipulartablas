$(function () {

    //boton borde rojo
    $('.bordesrojo').click(function () {
        $('table').css("border-color", "red");
    });

    //boton borde azul
    $('.bordesazul').click(function () {
        $('table').css("border-color", "blue");
    });

    //boton borde verde
    $('.bordesverde').click(function () {
        $('table').css("border-color", "green");
    });

    //boton eliminar
    $('.eliminar').click(function () {
        $('table').remove();
        //También se puede hacer con empty (vaciar)
        //$('table').empty();
    });

    // Boton añadir
    $('.anadir').click(function () {
        //seleccionamos la ultima fila, la clonamos y añadimos la copia a la tabla
        $('table tr:last-child').clone().appendTo('table');

        //otra forma
        // A la tabla añadimos la copia de la última fila
        //$('table').append($('table tr:last-child').clone());

    });

    //Boton restaurar
    $('.restaurar').click(function () {
        $('.tabla').html('<table border="1"><tr><td></td><td></td><td></td><td ></td ></tr ><tr><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td></tr></table > ');
    });



});